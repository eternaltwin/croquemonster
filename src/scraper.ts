import { AppDataSource } from './data-source';
import { looper } from './userScraper';

if (process.argv.length <= 2) {
	throw new Error('Missing SID');
}

const sid = process.argv[2];
const maxAccount = 634705;

AppDataSource.initialize()
	.then(() => {
		console.log('Data Source has been initialized successfully.');
		let i = 1;
		looper(i, sid, maxAccount);
	})
	.catch(err => {
		console.error('Error during Data Source initialization:', err);
	});

//1RAJNaRGxl0KaIeUVWJ8CssQh7Ko2yDy
