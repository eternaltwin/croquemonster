export class profile {
	userId: number; //OK
	name: string; //OK
	age: number; //OK
	level: number; //OK
	experience: number; //OK
	child: {
		scared: number; //OK
		eated: number; //OK
		innocent?: number; //OK
	};
	portals: number; //OK
	city: number; //OK
	contract: {
		easy: contract; //OK
		medium: contract; //OK
		hard: contract; //OK
		monstruous: contract; //OK
		infernal: contract; //OK
		total: contract; //OK
	};
	rewards: {};
	monster: Array<Monster>; //OK
	mummies: Array<Monster>;

	constructor(userId: number) {
		this.userId = userId;
	}

	/**
	 * calculateEaten
	 */
	public calculateEaten(text: string): number {
		if (text.match(/Tip.show\(null,\'dont un.*/)) {
			return 1;
		} else if (text.match(/Tip.show\(null,\'dont (\d*)/)) {
			return parseInt(text.match(/Tip.show\(null,\'dont (\d*)/)![1]);
		} else {
			return 0;
		}
	}
}

export class Monster {
	id: number; //OK
	name: string; //OK
	sadism: number; //OK
	laideur: number; //OK
	force: number; //OK
	gourmandise: number; //OK
	controle: number; //OK
	combat: number; //OK
	endurance: number; //OK
	prime: number; //OK
	love: Array<string>; //OK
	hate: Array<string>; //OK
	SWFObject: string; //OK
	mface: string; //OK
}

interface contract {
	victorious: number; //OK
	try: number; //OK
}
