import { Entity, PrimaryGeneratedColumn, ManyToOne, Relation, Column } from 'typeorm';
import { Player } from './player';

@Entity()
export class Reward {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Player, player => player.reward, {
		onDelete: 'CASCADE'
	})
	player: Relation<Player>;

	@Column({
		nullable: true
	})
	name: string; //OK

	@Column({
		nullable: true
	})
	quantity: number; //OK

	constructor(player: Player) {
		this.player = player;
	}
}
