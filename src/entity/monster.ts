import { Entity, PrimaryGeneratedColumn, ManyToOne, Relation, Column } from 'typeorm';
import { Player } from './player';

@Entity()
export class Monster {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Player, player => player.monster, {
		onDelete: 'CASCADE'
	})
	player: Relation<Player>;

	@Column({
		nullable: true
	})
	monsterId: number; //OK

	@Column({
		nullable: true
	})
	name: string; //OK

	@Column({
		nullable: true
	})
	sadism: number; //OK

	@Column({
		nullable: true
	})
	laideur: number; //OK

	@Column({
		nullable: true
	})
	force: number; //OK

	@Column({
		nullable: true
	})
	gourmandise: number; //OK

	@Column({
		nullable: true
	})
	controle: number; //OK

	@Column({
		nullable: true
	})
	combat: number; //OK

	@Column({
		nullable: true
	})
	endurance: number; //OK

	@Column({
		nullable: true
	})
	prime: number; //OK

	@Column('text', {
		nullable: true,
		array: true
	})
	love: Array<string>; //OK

	@Column('text', {
		nullable: true,
		array: true
	})
	hate: Array<string>; //OK

	@Column({
		nullable: true
	})
	SWFObject: string; //OK

	@Column({
		nullable: true
	})
	mface: string; //OK

	constructor(player: Player) {
		this.player = player;
	}
}
