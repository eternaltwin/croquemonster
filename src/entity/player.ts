import {
	Column,
	CreateDateColumn,
	Entity,
	JoinColumn,
	JoinTable,
	ManyToMany,
	ManyToOne,
	OneToMany,
	PrimaryGeneratedColumn,
	Relation,
	UpdateDateColumn
} from 'typeorm';
import { Contract } from './contract';
import { Monster } from './monster';
import { Reward } from './reward';
import { Children } from './children';

@Entity()
export class Player {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({
		nullable: true
	})
	userId: number; //OK

	@Column({
		nullable: true
	})
	name: string; //OK

	@Column({
		nullable: true
	})
	age: number; //OK

	@Column({
		nullable: true
	})
	level: number; //OK

	@Column({
		nullable: true
	})
	experience: number; //OK

	@Column({
		nullable: true
	})
	portals: number; //OK

	@Column({
		nullable: true
	})
	city: number; //OK

	@OneToMany(() => Contract, contract => contract.player, {
		cascade: true
	})
	contract: Relation<Contract[]>;

	@OneToMany(() => Children, children => children.player, {
		cascade: true
	})
	children: Relation<Children[]>;

	@OneToMany(() => Reward, reward => reward.player, {
		cascade: true
	})
	reward: Relation<Reward[]>;

	@OneToMany(() => Monster, monster => monster.player, {
		cascade: true
	})
	monster: Relation<Monster[]>;

	constructor(userId: number) {
		this.userId = userId;
	}

	/**
	 * calculateEaten
	 */
	public calculateEaten(text: string): number {
		if (text.match(/Tip.show\(null,\'dont un.*/)) {
			return 1;
		} else if (text.match(/Tip.show\(null,\'dont (\d*)/)) {
			return parseInt(text.match(/Tip.show\(null,\'dont (\d*)/)![1]);
		} else {
			return 0;
		}
	}
}
