import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, Relation } from 'typeorm';
import { Player } from './player';

export type contractType = 'easy' | 'medium' | 'hard' | 'monstruous' | 'infernal';
@Entity()
export class Contract {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Player, player => player.contract, {
		onDelete: 'CASCADE'
	})
	player: Relation<Player>;

	@Column({
		nullable: false,
		type: 'enum',
		enum: ['easy', 'medium', 'hard', 'monstruous', 'infernal']
	})
	type: contractType;

	@Column({
		nullable: true
	})
	victorious: number; //OK

	@Column({
		nullable: true
	})
	try: number; //OK

	constructor(player: Player, type: contractType, victorious: number, attempt: number) {
		this.player = player;
		this.type = type;
		this.victorious = victorious;
		this.try = attempt;
	}
}
