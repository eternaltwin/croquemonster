import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, Relation } from 'typeorm';
import { Player } from './player';

@Entity()
export class Children {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Player, player => player.children, {
		onDelete: 'CASCADE'
	})
	player: Relation<Player>;

	@Column()
	scared: number; //OK

	@Column()
	eated: number; //OK

	@Column()
	innocent: number; //OK

	constructor(player: Player) {
		this.player = player;
	}
}
