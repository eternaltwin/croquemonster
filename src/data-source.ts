import { getEnvironnement } from './context';
import dbConfig from './db.config';
import { DataSource } from 'typeorm';
import { Player } from './entity/player';
import { Monster } from './entity/monster';
import { Contract } from './entity/contract';
import { Reward } from './entity/reward';
import { Children } from './entity/children';

const dbConf = dbConfig(getEnvironnement());

export const AppDataSource = new DataSource({
	type: 'postgres',
	host: dbConf.HOST,
	port: 5433,
	username: dbConf.USER,
	password: dbConf.PASSWORD,
	database: dbConf.DB,
	synchronize: true, // Set to true if schema need to be updated in dev
	logging: false,
	entities: [Player, Monster, Contract, Reward, Children]
});
