import { writeFile } from 'fs';
import cliProgress from 'cli-progress';
import { Player } from './entity/player';
import { Monster } from './entity/monster';
import { Contract } from './entity/contract';
import { AppDataSource } from './data-source';
import { Reward } from './entity/reward';
import { sleep } from './sleep';
import { Children } from './entity/children';

const b1 = new cliProgress.SingleBar({
	format: 'progress [{bar}] {percentage}% | ETA: {eta}s | {value}/{total}',
	barCompleteChar: '\u2588',
	barIncompleteChar: '\u2591',
	hideCursor: true
});

export async function user(userId: number, sid: string) {
	if (userId === 291548) return; //No questions ask, it's a sblurbs page
	let text = await (
		await fetch(`http://www.croquemonster.com/user/${userId}`, {
			headers: {
				Accept: '*/*',
				Cookie: `sid=${sid}`,
				'Accept-Language': 'en-US,en;q=0.5',
				'User-Agent':
					'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Goanna/5.2 Firefox/68.0 Basilisk/20220806'
			}
		})
	).text();
	if (text.match(/Erreur Sadique/)) {
		return;
	}

	let user = new Player(userId);

	if (text.match(/Ouverture du fuseau horaire \d* en cours/)) {
		console.log(`sleeping`);
		await sleep(30000);
		text = await (
			await fetch(`http://www.croquemonster.com/user/${userId}`, {
				headers: {
					Accept: '*/*',
					Cookie: `sid=${sid}`,
					'Accept-Language': 'en-US,en;q=0.5',
					'User-Agent':
						'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Goanna/5.2 Firefox/68.0 Basilisk/20220806'
				}
			})
		).text();
	}
	try {
		user.name = text.match(/Agence de (.*)\n/)![1];
	} catch (err) {
		console.error(text);
	}
	if (user.name[0] === '#') {
		return;
	}
	user.age = parseInt(text.match(/<dd> (\d*) jour/)![1]);
	user.level = parseInt(text.match(/\t  <span class="level">(\d*)<\/span>/)![1]);
	try {
		user.experience = parseInt(text.match(/class="xpNb"> (\d*) %/)![1]);
	} catch (error) {
		user.experience = 0;
	}

	(user.portals = parseInt(text.match(/Nombre de portails :<\/dt>\n\t\t\t\t<dd>(\d*)<\/dd>/)![1])),
		(user.city = parseInt(text.match(/Villes contrôlées :<\/dt>\n\t\t\t\t<dd>(\d*) \/ 429.*<\/dd>/)![1]));

	await AppDataSource.manager.save(user);

	const children = new Children(user);
	(children.scared = parseInt(text.match(/Enfants effrayés :<\/dt>\n\t\t\t\t<dd> (\d*)<\/dd>/)![1])),
		(children.eated = parseInt(
			text.match(/\t\t\t\t<dt>Enfants dévorés :<\/dt>(:?\n|\t)*(?:<dd.*\(\);">|\n\t\t\t\t<dd>)(\d*)<\/dd>/)![2]
		)),
		(children.innocent = user.calculateEaten(text.match(/Enfants dévorés.*\n.*\n.*/)!.toString()));
	await AppDataSource.manager.save(children);
	const contracts = [
		new Contract(
			user,
			'easy',
			parseInt(text.match(/Contrats abordables :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![1]),
			parseInt(text.match(/Contrats abordables :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![2])
		),
		new Contract(
			user,
			'medium',
			parseInt(text.match(/Contrats normaux :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![1]),
			parseInt(text.match(/Contrats normaux :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![2])
		),
		new Contract(
			user,
			'hard',
			parseInt(text.match(/Contrats difficiles :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![1]),
			parseInt(text.match(/Contrats difficiles :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![2])
		),
		new Contract(
			user,
			'monstruous',
			parseInt(text.match(/Contrats monstrueux :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![1]),
			parseInt(text.match(/Contrats monstrueux :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![2])
		),
		new Contract(
			user,
			'infernal',
			parseInt(text.match(/Contrats infernaux :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![1]),
			parseInt(text.match(/Contrats infernaux :<\/dt>\n\t\t\t\t<dd>(\d*) \/ (\d*).*<\/dd>/)![2])
		)
	];

	await AppDataSource.manager.save(contracts);

	const rewardsArray = text.split('<div class="reward"');

	if (rewardsArray.length >= 2) {
		let rewardsToSave = [];
		for (let index = 1; index < rewardsArray.length; index++) {
			const reward = rewardsArray[index];
			const rewardToSave = new Reward(user);
			if (
				!reward.match(/\<img alt=".*" src="\/gfx\/rewards\/reward_(.*)\.gif".*/) &&
				!reward.match(/\<img alt=".*" src="\/gfx\/rewards\/(.*)\.gif".*/)
			) {
				try {
					rewardToSave.name = reward.match(/null,'(.*)',null/)![1];
				} catch (error) {
					console.log(reward, userId);
				}
			} else {
				try {
					rewardToSave.name = reward.match(/\<img alt=".*" src="\/gfx\/rewards\/reward_(.*)\.gif".*/)![1];
				} catch (err) {
					rewardToSave.name = reward.match(/\<img alt=".*" src="\/gfx\/rewards\/(.*)\.gif".*/)![1];
				}
			}

			let quantity = '';
			try {
				let test = reward.match(/<img alt="(\d*)" src="\/gfx\/font\/rewards\/\d*\.gif"\/>/g);
				test!.map(s => {
					quantity += s.match(/<img alt="(\d*)" src="\/gfx\/font\/rewards\/\d*\.gif"\/>/) ? [1] : 0;
				});
			} catch (error) {
				quantity = '0';
			}

			rewardToSave.quantity = parseInt(quantity);
			rewardsToSave.push(rewardToSave);
		}
		await AppDataSource.manager.save(rewardsToSave);
	}

	let monsterPage = await (
		await fetch(`http://www.croquemonster.com/user/${userId}/monsters`, {
			headers: {
				Accept: '*/*',
				Cookie: `sid=${sid}`,
				'Accept-Language': 'en-US,en;q=0.5',
				'User-Agent':
					'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Goanna/5.2 Firefox/68.0 Basilisk/20220806'
			}
		})
	).text();

	if (monsterPage.match(/De nombreux enfants sont effrayés en ce moment même !/)) {
		await sleep(2000);
		monsterPage = await (
			await fetch(`http://www.croquemonster.com/user/${userId}/monsters`, {
				headers: {
					Accept: '*/*',
					Cookie: `sid=${sid}`,
					'Accept-Language': 'en-US,en;q=0.5',
					'User-Agent':
						'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Goanna/5.2 Firefox/68.0 Basilisk/20220806'
				}
			})
		).text();
	}

	// let monster = monsterPage.match(/<div class="monsterBox">(.|\n)*?<\/div>\n\t\t<\/div>/)
	let monsterArray = monsterPage.split('<div class="monsterBox">');
	if (monsterArray.length >= 2) {
		let monsterToSave = [];
		for (let index = 1; index < monsterArray.length; index++) {
			const monsterTest = new Monster(user);
			const monster = monsterArray[index];
			monsterTest.name = monster.match(/<a href="\/monster\/(\d*)\/spy">(.*)<\/a>/)![2];
			monsterTest.id = parseInt(monster.match(/<a href="\/monster\/(\d*)\/spy">(.*)<\/a>/)![1]);
			monsterTest.SWFObject = monster.match(/(js.SWFObject.*)/)![0];
			monsterTest.mface = monster.match(/(swf.addVariable.*)/)![0].split("'")[3];
			monsterTest.sadism = parseInt(monster.match(/<li class=\"sadism\"><label>Sadisme<\/label> (\d*)<\/li>/)![1]);
			monsterTest.laideur = parseInt(monster.match(/<li class=\"ugliness\"><label>Laideur<\/label> (\d*)<\/li>/)![1]);
			monsterTest.force = parseInt(monster.match(/<li class=\"power\"><label>Force<\/label> (\d*)<\/li>/)![1]);
			monsterTest.gourmandise = parseInt(
				monster.match(/<li class=\"greediness\"><label>Gourmand<\/label> (\d*)<\/li>/)![1]
			);
			monsterTest.controle = parseInt(
				monster.match(/<li class=\"control\"><label.*>Contrôle<\/label> (\d*)<\/li>/)![1]
			);
			monsterTest.combat = parseInt(monster.match(/<li class=\"fight\"><label.*>Combat<\/label> (\d*)<\/li>/)![1]);
			monsterTest.endurance = parseInt(
				monster.match(/<li class=\"endurance\"><label.*>Endurance<\/label> (\d*)<\/li>/)![1]
			);
			monsterTest.prime = parseInt(monster.match(/<li class=\"bounty\">.*<\/label>(\d*)<\/li>/)![1]);
			monsterTest.love = monster
				.match(/<li class=\"like\">(.*)<\/li>/)![1]
				.split(',')
				.map(el => el.trim());
			monsterTest.hate = monster
				.match(/<li class=\"dislike\">(.*)<\/li>/)![1]
				.split(',')
				.map(el => el.trim());
			monsterToSave.push(monsterTest);
		}
		await AppDataSource.manager.save(monsterToSave);
	}
}

export async function looper(i: number, sid: string, maxAccount: number) {
	b1.start(maxAccount, 0, {
		speed: 'N/A'
	});
	while (i < maxAccount) {
		try {
			await user(i, sid);
		} catch (error) {
			console.error(error);
			// unknowError.push(i)
			writeFile(`error/${i}.json`, error, err => {
				if (err) console.log(err);
				else {
					console.log('File written successfully\n');
				}
			});
		}
		b1.increment(1);
		i++;
	}
	b1.stop();
}
